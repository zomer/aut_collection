import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Retrieves and removes the head of this queue,
 * or returns {@code null} if this queue is empty.
 *
 * @return the head of this queue, or {@code null} if this queue is empty
 */
public class TestQueuePollMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(2);
        queue.offer(1);
        queue.poll();
        assertEquals(0, queue.size());
    }

    @Test
    public void testEmptyQueue() {
        Queue queue = new TestQueue(2);
        queue.offer(1);
        queue.poll();
        assertTrue(queue.poll() == null);
    }
}
