import org.apache.commons.lang.ArrayUtils;

import java.util.*;

public class TestQueue implements Queue {

    private Object[] queue;
    private int maxSize; // максимальное количество элементов в очереди
    private int nElem;  // текущее количество элементов в очереди
    private int front;
    private int rear;

    public TestQueue(int maxSize) {
        this.maxSize = maxSize;
        queue = new Object[maxSize];
        rear = -1;
        front = 0;
        nElem = 0;
    }

    public int size() {
        return nElem;
    }

    public boolean isEmpty() {
        return nElem == 0;
    }

    public boolean contains(Object o) {

        if (o == null) {
            throw new NullPointerException();
        }

        return Arrays.stream(queue).anyMatch(e -> e != null && e.equals(o));
    }

    public Iterator iterator() {
        return Arrays.stream(queue).iterator();
    }

    public Object[] toArray() {
        return queue;
    }

    public Object[] toArray(Object[] a) {

        if (a == null) {
            throw new NullPointerException();
        }

        return Arrays.stream(queue)
                .filter(e -> Arrays.asList(a).contains(e))
                .map(e -> Arrays.asList(e).toArray())
                .findAny()
                .orElse(new Object [0]);
    }

    public boolean add(Object o) {

        if (o == null) {
            throw new NullPointerException();
        }

        if (nElem == maxSize) {
            throw new IllegalStateException();
        }

        if (rear == maxSize - 1) {  // циклический перенос
            rear = -1;
        }

        queue[++rear] = o;  //увеличение Rear и вставка
        nElem++;  // увеличение количества элементов в очереди
        return true;
    }

    public boolean remove(Object o) {
        if (nElem == 0) {
            return false;
        }

        Object temp = queue[front++]; // получаем первый элемент из очереди

        nElem--; // уменьшаем количество элементов в очереди
        return true;
    }

    public boolean addAll(Collection c) {

        if (c == null) {
            throw new NullPointerException();
        }

        if (c.size() + nElem >= maxSize) {
            throw new IllegalStateException();
        }

        nElem += c.size();  // увеличение количества элементов в очереди

        queue = ArrayUtils.addAll(c.toArray(), queue);
        return true;
    }

    public void clear() {
        nElem = 0;
        Arrays.fill(queue, null);
    }

    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException();
    }

    public boolean offer(Object o) {

        if (o == null) {
            throw new NullPointerException();
        }

        if (nElem == maxSize) {
            return false;
        }

        if (rear == maxSize - 1) {  // циклический перенос
            rear = -1;
        }

        queue[++rear] = o;  //увеличение Rear и вставка
        nElem++;  // увеличение количества элементов в очереди
        return true;
    }

    public Object remove() {

        if (nElem == 0) {
            throw new NoSuchElementException();
        }

        Object temp = queue[front++]; // получаем первый элемент из очереди

        if (front == maxSize) { // циклический перенос
            front = 0;
        }

        nElem--; // уменьшаем количество элементов в очереди

        return temp;
    }

    public Object poll() {

        if (nElem == 0) {
            return null;
        }

        Object temp = queue[front++]; // получаем первый элемент из очереди

        if (front == maxSize) { // циклический перенос
            front = 0;
        }

        nElem--; // уменьшаем количество элементов в очереди

        return temp;
    }

    public Object element() {

        if (nElem == 0) {
            throw new NoSuchElementException();
        }

        Object temp = queue[front++]; // получаем первый элемент из очереди

        if (front == maxSize) { // циклический перенос
            front = 0;
        }

        return temp;
    }

    public Object peek() {
        if (nElem == 0) {
            return null;
        }

        Object temp = queue[front++]; // получаем первый элемент из очереди

        if (front == maxSize) { // циклический перенос
            front = 0;
        }

        return temp;
    }
}
