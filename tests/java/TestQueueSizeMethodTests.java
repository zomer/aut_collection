import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Returns the number of elements in this collection.  If this collection
 * contains more than <tt>Integer.MAX_VALUE</tt> elements, returns
 * <tt>Integer.MAX_VALUE</tt>.
 *
 * @return the number of elements in this collection
 */
public class TestQueueSizeMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(2);
        queue.add("TEST");
        queue.add("TEST3");
        assertEquals(2, queue.size());
    }
}
