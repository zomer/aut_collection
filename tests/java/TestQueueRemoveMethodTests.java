import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Retrieves and removes the head of this queue.  This method differs
 * from {@link #poll poll} only in that it throws an exception if this
 * queue is empty.
 *
 * @return the head of this queue
 * @throws NoSuchElementException if this queue is empty
 */
public class TestQueueRemoveMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(2);
        queue.offer(1);
        queue.remove();
        assertEquals(0, queue.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void testEmptyQueue() {
        Queue queue = new TestQueue(2);
        queue.offer(1);
        queue.remove();
        queue.remove();
    }

    @Test
    public void testSizeBooleanReturn() {
        Queue queue = new TestQueue(2);
        queue.offer(1);
        assertTrue(queue.remove(1));
    }
}
