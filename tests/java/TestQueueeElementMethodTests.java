import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Retrieves, but does not remove, the head of this queue.  This method
 * differs from {@link #peek peek} only in that it throws an exception
 * if this queue is empty.
 *
 * @return the head of this queue
 * @throws NoSuchElementException if this queue is empty
 */
public class TestQueueeElementMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(1);
        queue.offer(1);
        assertEquals(1, queue.element());
    }

    @Test(expected = NoSuchElementException.class)
    public void testEmptyQueue() {
        Queue queue = new TestQueue(1);
        queue.element();
    }
}
