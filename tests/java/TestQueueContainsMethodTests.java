import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Returns <tt>true</tt> if this collection contains the specified element.
 * More formally, returns <tt>true</tt> if and only if this collection
 * contains at least one element <tt>e</tt> such that
 * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
 *
 * @param o element whose presence in this collection is to be tested
 * @return <tt>true</tt> if this collection contains the specified
 *         element
 * @throws ClassCastException if the type of the specified element
 *         is incompatible with this collection
 *         (<a href="#optional-restrictions">optional</a>)
 * @throws NullPointerException if the specified element is null and this
 *         collection does not permit null elements
 *         (<a href="#optional-restrictions">optional</a>)
 */
public class TestQueueContainsMethodTests {

    @Test
    public void testContains() {
        String test = "TEST";

        Queue queue = new TestQueue(2);
        queue.add(test);
        assertTrue(queue.contains(test));
    }

    @Test
    public void testNotContains() {
        Queue queue = new TestQueue(2);
        queue.add("TEST1");
        assertFalse(queue.contains("TEST2"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullPointerException() {
        Queue queue = new TestQueue(2);
        queue.add("TEST1");
        queue.contains(null);
    }
}
