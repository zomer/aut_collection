import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertTrue;


/**
 * Removes all of the elements from this collection (optional operation).
 * The collection will be empty after this method returns.
 *
 * @throws UnsupportedOperationException if the <tt>clear</tt> operation
 *         is not supported by this collection
 */
public class TestQueueClearMethodTests {

    @Test
    public void test() {
        Queue queue = new TestQueue(6);
        queue.add("TEST");
        queue.add("TEST2");
        queue.clear();
        assertTrue(queue.size() == 0);
    }
}
