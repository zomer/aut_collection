import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Returns an array containing all of the elements in this collection.
 * If this collection makes any guarantees as to what order its elements
 * are returned by its iterator, this method must return the elements in
 * the same order.
 *
 * <p>The returned array will be "safe" in that no references to it are
 * maintained by this collection.  (In other words, this method must
 * allocate a new array even if this collection is backed by an array).
 * The caller is thus free to modify the returned array.
 *
 * <p>This method acts as bridge between array-based and collection-based
 * APIs.
 *
 * @return an array containing all of the elements in this collection
 */
public class TestQueueToArrayMethodTests {

    @Test
    public void test() {
        String test = "test";
        String test2 = "test2";

        Queue queue = new TestQueue(2);
        queue.add(test);
        queue.add(test2);

        Queue queue2 = new TestQueue(2);
        queue2.add(test);
        queue2.add(test2);

        assertTrue(queue.toArray().equals(queue.toArray()));
    }

    @Test
    public void testWithData() {
        String test = "test";
        String test2 = "test2";

        Queue queue = new TestQueue(2);
        queue.add(test);
        queue.add(test2);

        assertTrue(queue.toArray(new Object[] {"test"}).length > 0);
    }
}
