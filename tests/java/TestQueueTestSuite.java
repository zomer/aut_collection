import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestQueueAddMethodTests.class,
        TestQueueeElementMethodTests.class,
        TestQueueOfferMethodTests.class,
        TestQueuePollMethodTests.class,
        TestQueueRemoveMethodTests.class,
        TestQueueePeekMethodTests.class,
        TestQueueSizeMethodTests.class,
        TestQueueIsEmptyMethodTests.class,
        TestQueueContainsMethodTests.class,
        TestQueueIteratorMethodTests.class,
        TestQueueAddAllMethodTests.class,
        TestQueueClearMethodTests.class,
        TestQueueContainsAllMethodTests.class,
        TestQueueRemoveAllMethodTests.class,
        TestQueueRetainAllMethodTests.class,
        TestQueueToArrayMethodTests.class

})
public class TestQueueTestSuite {
}
