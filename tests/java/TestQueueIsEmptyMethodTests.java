import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Returns <tt>true</tt> if this collection contains no elements.
 *
 * @return <tt>true</tt> if this collection contains no elements
 */
public class TestQueueIsEmptyMethodTests {

    @Test
    public void testNotEmpty() {
        Queue queue = new TestQueue(2);
        queue.add("test");
        assertFalse(queue.isEmpty());
    }

    @Test
    public void testEmpty() {
        Queue queue = new TestQueue(1);
        assertTrue(queue.isEmpty());
    }
}
