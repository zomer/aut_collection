import org.junit.Test;

import java.util.Queue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Inserts the specified element into this queue if it is possible to do
 * so immediately without violating capacity restrictions.
 * When using a capacity-restricted queue, this method is generally
 * preferable to {@link #add}, which can fail to insert an element only
 * by throwing an exception.
 *
 * @param e the element to add
 * @return {@code true} if the element was added to this queue, else
 *         {@code false}
 * @throws ClassCastException if the class of the specified element
 *         prevents it from being added to this queue
 * @throws NullPointerException if the specified element is null and
 *         this queue does not permit null elements
 * @throws IllegalArgumentException if some property of this element
 *         prevents it from being added to this queue
 */
public class TestQueueAddMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(2);
        queue.add("TEST");

        assertEquals(1, queue.size());
    }

    @Test
    public void testSizeSuccessInsert() {
        Queue queue = new TestQueue(2);
        assertTrue(queue.add("TEST") == true);
    }

    @Test(expected = IllegalStateException.class)
    public void testSizeUnsuccessfulInsert() {
        Queue queue = new TestQueue(1);
        queue.add("TEST");
        queue.add("TEST2");
    }
}
