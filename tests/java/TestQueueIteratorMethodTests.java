import org.junit.Test;

import java.util.Iterator;
import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Returns an iterator over the elements in this collection.  There are no
 * guarantees concerning the order in which the elements are returned
 * (unless this collection is an instance of some class that provides a
 * guarantee).
 *
 * @return an <tt>Iterator</tt> over the elements in this collection
 */
public class TestQueueIteratorMethodTests {

    @Test
    public void test() {
        Queue queue = new TestQueue(2);
        queue.add("TEST");
        assertTrue(Helper.isIteratorContains(queue.iterator(), "TEST"));
    }
}
