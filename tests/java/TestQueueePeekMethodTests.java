import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;



/**
 * Retrieves, but does not remove, the head of this queue,
 * or returns {@code null} if this queue is empty.
 *
 * @return the head of this queue, or {@code null} if this queue is empty
 */
public class TestQueueePeekMethodTests {

    @Test
    public void testSize() {
        Queue queue = new TestQueue(1);
        queue.offer(1);
        assertEquals(1, queue.peek());
    }

    @Test
    public void testEmptyQueue() {
        Queue queue = new TestQueue(1);
        assertTrue(queue.peek() == null);
    }
}
