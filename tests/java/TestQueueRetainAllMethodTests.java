import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertTrue;


/**
 * Retains only the elements in this collection that are contained in the
 * specified collection (optional operation).  In other words, removes from
 * this collection all of its elements that are not contained in the
 * specified collection.
 *
 * @param c collection containing elements to be retained in this collection
 * @return <tt>true</tt> if this collection changed as a result of the call
 * @throws UnsupportedOperationException if the <tt>retainAll</tt> operation
 *         is not supported by this collection
 * @throws ClassCastException if the types of one or more elements
 *         in this collection are incompatible with the specified
 *         collection
 *         (<a href="#optional-restrictions">optional</a>)
 * @throws NullPointerException if this collection contains one or more
 *         null elements and the specified collection does not permit null
 *         elements
 *         (<a href="#optional-restrictions">optional</a>),
 *         or if the specified collection is null
 * @see #remove(Object)
 * @see #contains(Object)
 */
public class TestQueueRetainAllMethodTests {

    @Test(expected = UnsupportedOperationException.class)
    public void test() {
        Queue queue = new TestQueue(6);
        queue.add("TEST");
        queue.retainAll(null);
    }
}
