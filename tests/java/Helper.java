import java.util.Collection;
import java.util.Iterator;

/**
 * Created by marg on 22.11.16.
 */
public class Helper {
    
    public static boolean isIteratorContains(Iterator iterator, String val) {
        while (iterator.hasNext()) {
            if (iterator.next().equals(val)) {
                return true;
            }
        }
        return false;
    }
}
